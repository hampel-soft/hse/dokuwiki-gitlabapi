# dokuwiki-gitlabapi


## Description

Dokuwiki Plugins for displaying gitlab issues and gitlab tags.

### Features

* Display all issues with the label `external` for a given namespace and project
* Display all tags and corresponding _releases_ for a given namespace and project
  * Releases are files that are listed als downloads for a given tag. The `gitlabtags` plugin searches for a directory on a local or a remote webserver (see configuration below) and displays download links for all files found in that directory.

## Download and Installation

Copy all three contained directories (`/gitlabhelper`, `/gitlabissues` and `/gitlabtags`) to Dokuwiki's `lib/plugins` directory. Refer to https://www.dokuwiki.org/plugins for more information on how to install plugins manually.


## Configuration and Settings

All settings are available on the standard DokuWiki “Settings” page on the "Gitlabhelper" section:

* **gitlab_url**: URL of the Gitlab server API (default: `https://gitlab.com/api/v3`)
* **api_token**: Private token of the user that should access gitlab application resources
* **release_url**: URL to the directory containing released files. This is the base URL for serving files. An example download link could look like this: `release_url/namespace/project/tag/file.zip`
* **release_remote**: Check for released files on a remote server? gitlabhelper can either browse a directory on the same server that dokuwiki is installed on, or it can communicate with another server hosting the release files. If you check this option, gitlabhelper will try to read files from the `release_url` defined above. There has to be a script at this location handling two requests: `release_url/index.php?l=...` which returns a list of files for a given directory, and `release_url/index.php?d=...` which returns the file itself for download. A sample `index.php.sample` can be found in the gitlabhelper plugin directory.
* **remote_pubkey**: If checking remotely: Public key for secure communication. This parameter is _optional_. If omitted, the URLs are (only) base64 encoded
* **release_path**: If checking locally: Server path to the directory containing released files. This is the directory that resembles the same location as the `release_url` above. An example file could reside at: `release_path/namespace/project/tag/file.zip`
* **use_namespace**: If checking locally: Use namespace in path and url? I.e. put it between the `release_path` / `release_url` and the project name?


## Example

### Pre-Requisites:

The following settings are just made up to illustrate a _real-world example_.

- There is a namespace named `joerg.hampel`
- There is a project named `dokuwiki-gitlabapi`
- There is a tag named `v0.1.0`
- There is a file named `release_v010.zip` you want to release


### Release files are hosted on same (local) server as dokuwiki

- The base url of your release file folder structure is `http://www.hampel.at/releases/`
- The base directory of your release files on your local webserver is `/home/www/releases`

#### Use namespace for releases

If you want to use the namespace in path and url, you would configure:

```
use_namespace = true
release_path = /home/www/releases
release_url = http://www.hampel.at/releases/
```

You then would upload the file `release_v010.zip` to the directory   
`/home/www/releases/joerg.hampel/dokuwiki-gitlabapi/v0.1.0/`

The gitlabtags plugin would then show the tag `v0.1.0` and a download link to   
http://www.hampel.at/releases/joerg.hampel/dokuwiki-gitlabapi/v0.1.0/release_v010.zip


#### Don't use namespace for releases

If you do not want to use the namespace in path and url, you would configure:

```
use_namespace = false
release_path = /home/www/releases
release_url = http://www.hampel.at/releases/
```

You then would upload the file `release_v010.zip` to the directory   
`/home/www/releases/dokuwiki-gitlabapi/v0.1.0/`

The gitlabtags plugin would then show the tag `v0.1.0` and a download link to   
http://www.hampel.at/releases/dokuwiki-gitlabapi/v0.1.0/release_v010.zip

### Release files are hosted on different (remote) server

- The remote server is reachable via `download.hampel.at` and files should be hosted in subdirectory `/releases`
- On the remote server, the directory that maps to the above mentioned subdirectory is `/var/services/web/releases`

You would configure:

```
release_url = http://download.hampel.at/releases/
release_remote = true
```
On your remote server, you need to copy the `index.php` file to the `releases` 
directory. The `index.php` file provides the `gitlabhelper` plugin with info on 
which files reside within a given directory, and it handles file downloads via
a base64-encoded download url sent as HTTP GET parameter (see below).

You then would upload the file `release_v010.zip` on your remote server to the directory 
`/var/services/web/releases/joerg.hampel/dokuwiki-gitlabapi/v0.1.0/`

The gitlabtags plugin would then show the tag `v0.1.0` and a download link to
http://download.hampel.at/releases/?d=<%base64-encoded-download-url%>


## Usage

### Issues

Put the following code on a dokuwiki page and replace `[namespace]` and `[projectname]` with your actual data:
```
~~NOCACHE~~
{(gitlabissues>[namespace]/[projectname])}
```

Example:

```
~~NOCACHE~~
{(gitlabissues>joerg.hampel/dokuwiki-gitlabhelper)}
```


### Tags and Releases

Put the following code on a dokuwiki page and replace `[namespace]` and `[projectname]` with your actual data:

```
~~NOCACHE~~
{(gitlabtags>[namespace]/[projectname])}
```

Example:

```
~~NOCACHE~~
{(gitlabtags>joerg.hampel/dokuwiki-gitlabhelper)}
```

