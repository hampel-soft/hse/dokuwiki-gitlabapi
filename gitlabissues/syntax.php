<?php
/**
 * DokuWiki Plugin gitlabissues (Syntax Component)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  Joerg Hampel <joerg@hampel.at>
 */

// must be run within Dokuwiki
if (!defined('DOKU_INC')) die();

if(!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
require_once(DOKU_PLUGIN.'syntax.php');

class syntax_plugin_gitlabissues extends DokuWiki_Syntax_Plugin {

	private $labels = array();

    /**
     * @return string Syntax mode type
     */
    function getType() {
        return 'substition';
    }
    /**
     * @return string Paragraph type
     */
    function getPType() {
        return 'block';
    }
    /**
     * @return int Sort order - Low numbers go before high numbers
     */
    function getSort() {
        return 990;
    }

    /**
     * Connect lookup pattern to lexer.
     *
     * @param string $mode Parser mode
     */
    function connectTo($mode) {
        $this->Lexer->addSpecialPattern('\{\(gitlabissues>[^}]*\)\}',$mode,'plugin_gitlabissues');
    }


    /**
     * Handle matches of the gitlabissues syntax
     *
     * @param string $match The match of the syntax
     * @param int    $state The state of the handler
     * @param int    $pos The position in the document
     * @param Doku_Handler    $handler The handler
     * @return array Data for the renderer
     */
    function handle($match, $state, $pos, Doku_Handler $handler) {
        
        // get project-name from params
        $match = substr($match,strlen('{(gitlabissues>'),-2); //strip markup from start and end
        $params = explode(",",$match); //split parameter string
        
        return $params;
    }


    /**
     * Render xhtml output or metadata
     *
     * @param string         $mode      Renderer mode (supported modes: xhtml)
     * @param Doku_Renderer  $renderer  The renderer
     * @param array          $data      The data from the handler() function
     * @return bool If rendering was successful.
     */
	function render($mode, Doku_Renderer $renderer, $data) {
		if($mode != 'xhtml') return false;

		$gitlabhelper = $this->loadHelper('gitlabhelper'); // or $this->loadHelper('tag', true);

		// get project-id from project-name (in data array)
		$proj = $gitlabhelper->getGitlabObject('project', $data[0]);
		if($proj->path_with_namespace == $data[0])
		{
			// include fontawesome for tag icon
			$renderer->doc .= '<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">';

			$projectid = $proj->id;
			$projectpath = $proj->path_with_namespace;

			// get label names and colors
			$this->labels = $gitlabhelper->getGitlabObject('labels', $projectid);


			switch($_GET['issuetodo']) {

				// ======================================================================================
				// DISPLAY FORM FOR ISSUE CREATION
				// ======================================================================================
				case "newissue":
					$renderer->doc .= "<p class='gitlablinks'>Creating a new issue for the <a href='https://gitlab.com/"
									. $projectpath . "'><i class='fa fa-lock'></i> " . $projectpath . "</a> gitlab project.</p>";

					$renderer->doc .= "<form class='gitlabform' action='?issuetodo=submitissue' method='post'>";
					$renderer->doc .= "<input type='hidden' name='issuepid' value='" . $projectid . "' />";
					$renderer->doc .= "<table>";
					$renderer->doc .= "<tr><th>Title</th><td colspan='2'><input type='text' name='issuetitle' class='gitlabinput' /></td></tr>";
					$renderer->doc .= "<tr><th>Description</th><td colspan='2'><textarea type='text' name='issuedesc' rows='10' class='gitlabinput'></textarea></td></tr>";
					$renderer->doc .= "<tr><td>&nbsp;</td><td><input type='submit' value='Create Issue' /></td>";
					$renderer->doc .= "<td style='text-align: right'><small><a href='http://doc.gitlab.com/ee/markdown/markdown.html' target='_blank'>Use Markdown Syntax</a></small></td></tr>";
					$renderer->doc .= "</table></form>";

					$renderer->doc .= "<p class='gitlablinks'>Want to cancel? Return to <a href='?issuestate=open'><i class='fa fa-list-ul'></i> list of issues</a>.</p>";
					break;

				// ======================================================================================
				// DISPLAY RESULT OF ISSUE CREATION
				// ======================================================================================
				case "submitissue":
					$renderer->doc .= "<p class='gitlablinks'>Creating a new issue for the <a href='https://gitlab.com/"
									. $projectpath . "'><i class='fa fa-lock'></i> " . $projectpath . "</a> gitlab project.</p>";

					$resstr = $gitlabhelper->createNewIssue($_POST['issuepid'],$_POST['issuetitle'] . " [" . $_SERVER['REMOTE_USER'] . "]",$_POST['issuedesc']);
					$json = new JSON();
					$resobj = $json->decode($resstr);
					if($resobj->project_id == $projectid)
						$renderer->doc .= "<p><i class='fa fa-check'></i> New issue created successfully!</p>";
					else
						$renderer->doc .= "<p><i class='fa fa-close'></i> Creating issue failed (" . $resstr . ").</p>";
					$renderer->doc .= "<p class='gitlablinks'>Return to <a href='?issuestate=open'><i class='fa fa-list-ul'></i> list of issues</a>.</p>";
					break;

				// ======================================================================================
				// DISPLAY FORM FOR COMMENT (NOTES) CREATION
				// ======================================================================================
				case "newcomment":
					$renderer->doc .= "<p class='gitlablinks'>Creating a new comment for <strong>issue #" . $_GET['issueiid'] . "</strong> of the <a href='https://gitlab.com/"
									. $projectpath . "'><i class='fa fa-lock'></i> " . $projectpath . "</a> gitlab project.</p>";

					$renderer->doc .= "<form class='gitlabform' action='?issuetodo=submitcomment' method='post'>";
					$renderer->doc .= "<input type='hidden' name='issuepid' value='" . $projectid . "' />";
					$renderer->doc .= "<input type='hidden' name='issueid' value='" . $_GET['issueid'] . "' />";
					$renderer->doc .= "<input type='hidden' name='issueiid' value='" . $_GET['issueiid'] . "' />";
					$renderer->doc .= "<table>";
					$renderer->doc .= "<tr><th>Comment</th><td colspan='2'><textarea type='text' name='issuebody' rows='10' class='gitlabinput'></textarea></td></tr>";
					$renderer->doc .= "<tr><td>&nbsp;</td><td><input type='submit' value='Create Comment' /></td>";
					$renderer->doc .= "<td style='text-align: right'><small><a href='http://doc.gitlab.com/ee/markdown/markdown.html' target='_blank'>Use Markdown Syntax</a></small></td></tr>";
					$renderer->doc .= "</table></form>";

					$renderer->doc .= "<p class='gitlablinks'>Want to cancel? Return to <a href='?issuestate=open'><i class='fa fa-list-ul'></i> list of issues</a>.</p>";
					break;

				// ======================================================================================
				// DISPLAY RESULT OF COMMENT CREATION
				// ======================================================================================
				case "submitcomment":
					$renderer->doc .= "<p class='gitlablinks'>Creating a new comment for <strong>issue #" . $_POST['issueiid'] . "</strong> of the <a href='https://gitlab.com/"
									. $projectpath . "'><i class='fa fa-lock'></i> " . $projectpath . "</a> gitlab project.</p>";

					$resstr = $gitlabhelper->createNewComment($_POST['issuepid'],$_POST['issueid'], "[" . $_SERVER['REMOTE_USER'] . "] " . $_POST['issuebody']);
					$json = new JSON();
					$resobj = $json->decode($resstr);
					if($resobj->id != "")
						$renderer->doc .= "<p><i class='fa fa-check'></i> New Comment created successfully!</p>";
					else
						$renderer->doc .= "<p><i class='fa fa-close'></i> Creating comment failed (" . $resstr . ").</p>";
					$renderer->doc .= "<p class='gitlablinks'>Return to <a href='?issuestate=open'><i class='fa fa-list-ul'></i> list of issues</a>.</p>";
					break;

				// ======================================================================================
				// DISPLAY ISSUE LIST
				// ======================================================================================
				default:
					$renderer->doc .= $this->createLinks($projectpath, $_GET['issuestate']);

					//get all issues
					if($_GET['issuestate'] == 'closed')
						$issues = $gitlabhelper->getGitlabObject('issuesclosed', $projectid);
					else
						$issues = $gitlabhelper->getGitlabObject('issues', $projectid);

					if($issues->message) {
						$renderer->doc .= "<p>Fetching issues for project <code>" . $data[0] . "</code> from server failed: <code>" . $issues->message . "</code></p>";
						return false;
					}

					if (is_array($issues) && (sizeof($issues)>0)) {
						$renderer->doc .= "<table class='gitlabapi'>";
						foreach($issues as &$val) {
							//get id, title, description
							$renderer->doc .= "<tr><th class='leftcol'><a name='" . $val->iid . "'>#" . $val->iid . "</a></th>";
							$renderer->doc .= "<th colspan='2'>" . $val->title . "</th></tr>";
							$renderer->doc .= "<tr><td rowspan='4'>&nbsp;</td>";
							$renderer->doc .= "<td colspan='2'>" . $gitlabhelper->formatText($val->description) . "</td></tr>";

							//get comments
							$renderer->doc .= "<tr><td colspan='2'>";
							$comments = $gitlabhelper->getGitlabObject('notes', $projectid, $val->id);
							if (is_array($comments) && (sizeof($comments)>0)) {
								$renderer->doc .= "<ul>";
								foreach($comments as &$commval) {
									$renderer->doc .= "<li><small><i class='fa fa-comment'></i> <i>" . $commval->author->name . ", ";
									$renderer->doc .= $gitlabhelper->cleandate($commval->created_at) . ":</i></small></br>";
									$renderer->doc .= $gitlabhelper->formatText($commval->body) . "</li>";
								}
								$renderer->doc .= "</ul><br/>";
							}
							$renderer->doc .= "<small><a href='?issuetodo=newcomment&issueid=" . $val->id . "&issueiid=" . $val->iid . "'><i class='fa fa-comment-o'></i> Add new comment</a></small>";
							$renderer->doc .= "</td></tr>";


							//get dates, state, assignee
							$renderer->doc .= "<tr><td class='half grey'><small>Created: " . $gitlabhelper->cleandate($val->created_at) . "</small></td>";
							//get labels
							$renderer->doc .= "<td class='half grey'><small>Labels: ";
							if (is_array($val->labels) && (sizeof($val->labels)>0)) {
								foreach($val->labels as &$labelval) {
									if ($labelval != "external")
										$renderer->doc .= $this->formatLabel($labelval) . "&nbsp;";
								}
							}
							$renderer->doc .= "</small></td></tr>";
							$renderer->doc .= "<tr><td class='half grey'><small>Updated: " . $gitlabhelper->cleandate($val->updated_at) . "</small></td>";
							$renderer->doc .= "<td class='half grey'><small>Assignee: " . $val->assignee->name . "</small></td></tr>";
							$renderer->doc .= "<tr><td colspan='3'></td></tr>";
						}
						$renderer->doc .= "</table> ";

						$renderer->doc .= $this->createLinks($projectpath, $_GET['issuestate']);
					}
					else {
						$renderer->doc .= "<p>No issues found for project <code>" . $data[0] . "</code> on server.</p>";
					}
			}
		}
		else {
			$renderer->doc .= "<p>Project <code>" . $data[0] . "</code> not found on server.</p>";
		}
		return true;
	}



    /**
     * format label with background-color
     *
     * @param $labelname
     * @return string
     */
    private function formatLabel($labelname) {
		$labelcolor = "#cccccc";
		foreach ($this->labels as $lbl) {
			if ($lbl->name == $labelname)
				$labelcolor = $lbl->color;
		}
		$output = "<span style='color:#fff;background:" . $labelcolor . "'>&nbsp;" . $labelname . "&nbsp;</span>";
		return $output;
    }



    /**
     * create output for head/foot links
     *
     * @param $projectpath
     * @param $issuestate
     * @return string
     */
    private function createLinks($projectpath, $issuestate) {
		$output  = "<p class='gitlablinks'>Showing <b>" . ($issuestate == 'closed' ? 'closed' : 'open') . "</b> issues for the ";
		$output .= "<a href='https://gitlab.com/" . $projectpath . "'><i class='fa fa-lock'></i> " . $projectpath . "</a> project.<br/>";
		$output .= "<a href='?issuestate=" 
				. ($issuestate == 'closed' ? 'open' : 'closed')
				. "'><i class='fa fa-eye-slash'></i> Show "
				. ($issuestate == 'closed' ? 'open' : 'closed')
				. " issues</a> or <a href='?issuetodo=newissue'><i class='fa fa-plus-square-o'></i> add new issue</a>.</p>";
		return $output;
    }



}
// vim:ts=4:sw=4:et: