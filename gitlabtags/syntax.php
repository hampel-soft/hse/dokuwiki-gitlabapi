<?php
/**
 * DokuWiki Plugin gitlabtags (Syntax Component)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  Joerg Hampel <joerg@hampel.at>
 */

// must be run within Dokuwiki
if (!defined('DOKU_INC')) die();

if(!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
require_once(DOKU_PLUGIN.'syntax.php');

class syntax_plugin_gitlabtags extends DokuWiki_Syntax_Plugin {

    /**
     * @return string Syntax mode type
     */
    function getType() {
        return 'substition';
    }
    /**
     * @return string Paragraph type
     */
    function getPType() {
        return 'block';
    }
    /**
     * @return int Sort order - Low numbers go before high numbers
     */
    function getSort() {
        return 991;
    }

    /**
     * Connect lookup pattern to lexer.
     *
     * @param string $mode Parser mode
     */
    function connectTo($mode) {
        $this->Lexer->addSpecialPattern('\{\(gitlabtags>[^}]*\)\}',$mode,'plugin_gitlabtags');
    }


    /**
     * Handle matches of the gitlabissues syntax
     *
     * @param string $match The match of the syntax
     * @param int    $state The state of the handler
     * @param int    $pos The position in the document
     * @param Doku_Handler    $handler The handler
     * @return array Data for the renderer
     */
    function handle($match, $state, $pos, Doku_Handler $handler) {
        
        // get project-name from params
        $match = substr($match,strlen('{(gitlabtags>'),-2); //strip markup from start and end
        $params = explode(",",$match); //split parameter string
        return $params;
    }


    /**
     * Render xhtml output or metadata
     *
     * @param string         $mode      Renderer mode (supported modes: xhtml)
     * @param Doku_Renderer  $renderer  The renderer
     * @param array          $data      The data from the handler() function
     * @return bool If rendering was successful.
     */
	function render($mode, Doku_Renderer $renderer, $data) {
		if($mode != 'xhtml') return false;

		$gitlabhelper = $this->loadHelper('gitlabhelper'); // or $this->loadHelper('tag', true);

		// get project-id from project-name (in data array)
		$proj = $gitlabhelper->getGitlabObject('project', $data[0]);
		if($proj->path_with_namespace == $data[0])
		{
			$projectid = $proj->id;
			$namespace = $proj->namespace->full_path;
			$project   = $proj->path;
			$fullpath  = $namespace . "/" . $project;
			
			// include fontawesome for tag icon
			$renderer->doc .= '<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">';

			$renderer->doc .= "<p class='gitlablinks'>Showing the last 10 releases for the ";
			$renderer->doc .= "<a href='https://gitlab.com/" . $fullpath . "'><i class='fa fa-cog'></i> " . $fullpath . "</a> project.<br/>";

			// get tags
			if($tags = $gitlabhelper->getGitlabObject('tags', $projectid)) {
				if($tags->message) {
					$renderer->doc .= "<p>Fetching releases for project <code>" . $data[0] . "</code> from server failed: <code>" . $tags->message . "</code></p>";
					return false;
				}
				foreach($tags as &$tag) {
					$renderer->doc .= "<h2 class='gitlabtags'><i class='fa fa-tag'></i> " . $tag->name . "</h2>";

					$renderer->doc .= "<p class='gitlabtaginfo'><i class='fa fa-cog'></i> <tt>" . substr($tag->commit->id, 0, 8) . "</tt>";
					$renderer->doc .= " by " . $tag->commit->committer_name . " (" . $tag->commit->committer_email . ")";
					$renderer->doc .= " on " . $gitlabhelper->cleanDate($tag->commit->committed_date) . "</p>";

					$renderer->doc .= "<h3 class='gitlabtags'>Release Notes</h3>";
					$renderer->doc .= "<p class='gitlabtagmsg'>" . $gitlabhelper->formatText($tag->message) . "</p>";
					/*
					$pos1 = stripos($tag->commit->message, "Signed-off-by") - 2;
					$cutmsg = ($pos1 > 0 ? substr($tag->commit->message, 0, $pos1) : $tag->commit->message);
					$renderer->doc .= "<p class='gitlabtagmsg'>" . $gitlabhelper->formatText($cutmsg) . "</p>";
					*/

					if($files = $gitlabhelper->getReleaseFiles($namespace, $project, $tag->name)) {
						$renderer->doc .= "<h3 class='gitlabtags'>Downloads</h3>";
						$renderer->doc .= "<table class='gitlabtags'>";
						foreach($files as $file) {
							$renderer->doc .= "<tr><td><a href='" . $gitlabhelper->getReleaseUrl($namespace, $project, $tag->name, $file[0]) . "'>";
							$renderer->doc .= "<i class='fa fa-lock'> " . $file[0] . "</a></td>";
							$renderer->doc .= "<td class='size'>" . $file[1] . "</td></tr>";
						}
						$renderer->doc .= "</table>";
					}
					else {
						$renderer->doc .= "<p><small><strong>No downloads available.</strong></small></p>";
					}
				}
			}
			else {
				$renderer->doc .= "<p>No releases found for project <code>" . $data[0] . "</code> on server.</p>";
			}
		}
		else {
			$renderer->doc .= "<p>Project <code>" . $data[0] . "</code> not found on server.</p>";
		}

		return true;
	}


}
// vim:ts=4:sw=4:et:
